using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject startMenuPanel;
    public GameObject gameModePanel;
    public GameObject selectMapPanel;
    public AudioSource buttonClickSound;

    private GameObject previousPanel;

    private void Start()
    {
        startMenuPanel.SetActive(true);
        gameModePanel.SetActive(false);
        selectMapPanel.SetActive(false);
    }

    public void Play()
    {
        startMenuPanel.SetActive(false);
        gameModePanel.SetActive(true);
        buttonClickSound.Play();
    }

    public void Practice()
    {
        SceneManager.LoadScene("Training");
        buttonClickSound.Play();
    }

    public void Competition()
    {
        gameModePanel.SetActive(false);
        selectMapPanel.SetActive(true);
        previousPanel = gameModePanel;
        buttonClickSound.Play();
    }

    public void Exit()
    {
        Application.Quit();
        buttonClickSound.Play();
    }

    public void Back()
    {
        if (previousPanel != null)
        {
            previousPanel.SetActive(false);
            previousPanel = null;
        }
        buttonClickSound.Play();
    }
}
